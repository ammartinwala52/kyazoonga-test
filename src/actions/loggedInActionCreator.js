function updateLoggedIn(loggedIn) {
    return (dispatch) => {
      dispatch({
        type: 'LOGGED_IN',
        payload: loggedIn,
      });
    };
  }
  
  export default updateLoggedIn;
  