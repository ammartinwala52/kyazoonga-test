import React, { Component } from 'react';
import ticketData from '../../test-data/tickets.json';

class Tickets extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ticketData: [],
    };
  }

  componentDidMount() {
    this.setState({
      ticketData,
    });
  }

  render() {
    const { ticketData } = this.state;
    return (
      <section className="hot-ticket-sec ticket-boxes">
        <div className="container">
          <h2 className="site-title">
            Hot Tickets
            <small>Get tickets to your favourite events</small>
          </h2>
          <div className="row">
            {ticketData.map((data, index) => {
              return (
                <div key={index} className="col-xs-12 col-sm-6 col-lg-4">
                  <div className="box">
                    {Object.keys(data.attendingList).length > 0 ? (
                      <div className="dropdown visiter-icon">
                        <a href="#" className="dropdown-toggle" type="button" data-toggle="dropdown">
                          <span className="glyphicon glyphicon-user" aria-hidden="true" />
                        </a>
                        <div className="dropdown-menu">
                          <p>{data.attendingList.description}</p>
                          {data.attendingList.list.map((listData, listIndex) => {
                            return (
                              <a href="#" key={listIndex}>
                                <img src={listData.src} alt="" />{' '}
                              </a>
                            );
                          })}
                        </div>
                      </div>
                    ) : (
                      <div />
                    )}
                    <a href="#" className="block-link">
                      <div className="ticket-img">
                        <img src={data.imgSrc} alt="" />
                        <span className="ticket-price">{data.price}</span>
                      </div>
                      <div className="ticket-info">
                        <span className="time-info">{data.info}</span>
                        <h3 className="ticket-title">{data.title}</h3>
                        <p className="venue-info">{data.venue}</p>
                      </div>
                    </a>
                    <div className="social clearfix">
                      <div className="pull-left">
                        <a className="btn btn-default btn-xs ical" href="#">
                          <i className="fa fa-calendar" /> &nbsp;iCal
                        </a>
                        <a className="btn btn-default btn-xs ml-10" href="#">
                          <i className="fa fa-calendar" /> &nbsp;Google
                        </a>
                      </div>
                      <div className="pull-right">
                        <a className="btn btn-default btn-xs btn-circle" href="#">
                          <i className="fa fa-facebook" />
                        </a>
                        <a className="btn btn-default btn-xs btn-circle" href="#">
                          <i className="fa fa-twitter" />
                        </a>
                        <a className="btn btn-default btn-xs btn-circle" href="#">
                          <i className="fa fa-twitter" />
                        </a>
                        <a className="btn btn-default btn-xs btn-circle" href="#">
                          <i className="fa fa-linkedin" />
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
          <div className="btn-sec text-center p-20 pt-0">
            <a href="#" className="btn btn-primary btn-custome-lg">
              Discover More
            </a>
          </div>
        </div>
      </section>
    );
  }
}

export default Tickets;
