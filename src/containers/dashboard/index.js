import React, { Component } from 'react';

import Header from '../header';
import Slider from '../slider';
import Tickets from '../tickets';
import Categories from '../categories';
import Promotion from '../promotion';
import Featured from '../featured';
import Footer from '../footer';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <Header />
        <Slider />
        <Tickets />
        <Categories />
        <Promotion />
        <Featured />
        <Footer />
      </div>
    );
  }
}

export default Dashboard;
