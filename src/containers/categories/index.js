import React, { Component } from 'react';
import categoriesData from '../../test-data/categories.json';

class Categories extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categoriesData: [],
    };
  }

  componentDidMount() {
    this.setState({
      categoriesData,
    });
  }

  render() {
    const { categoriesData } = this.state;
    return (
      <section className="home-cat-sec text-center">
        <div className="container">
          <h2 className="site-title">Browse by top categories</h2>
          <div className="row">
            {categoriesData.map((data, index) => {
              return (
                <div className="col-sm-4 cat-box" key={index}>
                  <a href="#" className="block-link">
                    <div className="cat-txt">
                      <h3>{data.name}</h3>
                    </div>
                    <img src={data.imgSrc} alt="" className="img-full-width" />
                  </a>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    );
  }
}

export default Categories;
