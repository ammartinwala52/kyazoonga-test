import React, { Component } from 'react';
import featuredData from '../../test-data/featured.json';

class Featured extends Component {
  constructor(props) {
    super(props);

    this.state = {
      featuredData: [],
    };
  }

  componentDidMount() {
    this.setState({
      featuredData,
    });
  }

  render() {
    const { featuredData } = this.state;
    return (
      <section className="home-featured-sec text-center">
        <h2 className="site-title">
          <span>We've been featured in</span>
        </h2>
        <div className="container-fluid">
          {featuredData.map((data, index) => {
            return (
              <a target="_blank" href={data.mainLink} key={index}>
                <img src={data.imgSrc} alt="" />
              </a>
            );
          })}
        </div>
      </section>
    );
  }
}

export default Featured;
