import React from 'react';
import loader from '../assets/css/load-icon.gif';

const Loader = () => (
  <div id="page-loader">
    <div className="loader">
      <img src={loader} alt="Loading..." />
    </div>
  </div>
);

export default Loader;