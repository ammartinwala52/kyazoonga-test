import React, { Component } from 'react';

import menuData from '../../test-data/menu.json';
import sliderData from '../../test-data/slider.json';

class Slider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menuData: [],
      sliderData: sliderData,
    };
  }

  searchfor = () => {
    //search
  };

  componentDidMount() {
    this.setState({
      menuData,
    });
  }
  render() {
    const { menuData, sliderData } = this.state;
    return (
      <div id="myCarousel" className="carousel slide carousel-fade home-slider" data-ride="carousel">
        <h1>Tickets made simple for everyone, everywhere</h1>
        <div className="site-manu container">
          <div className="collapse navbar-collapse hide-on-mobile">
            <ul className="nav navbar-nav sub-main-nav">
              {menuData.map((data, index) => {
                return (
                  <li key={index}>
                    <a href="#" className={data.class}>
                      {data.menu}
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
          <div className="banner-serch-box">
            <input
              id="txtSearchBox"
              className=""
              name="q"
              defaultValue=""
              placeholder="Search for an event, venue or city"
              type="text"
            />
            <a href="javascript:void(0);" onClick={this.searchfor}>
              <i className="fa fa-search" aria-hidden="true" />
            </a>
          </div>
        </div>
        {/* <!-- Carousel items --> */}
        <div className="carousel-inner">
          {/* <!-- slide #1 --> */}
          {/* {sliderData.map((data, index) => {
            return (
              <div key={index} data-slide={index} className="item">
                <img src={data.imgUrl} alt="" className="hide-on-desktop" />>
                <div className="hide-on-mobile">
                  <video title={data.title} id={data.id} autoPlay loop muted poster={data.posterUrl}>
                    <source src={data.videoUrl} type="video/mp4" />
                    >Your browser does not support the video tag.
                  </video>
                </div>
              </div>
            );
          })} */}
          <div data-slide="0" className="item active">
            <img src="http://content.kyazoonga.com/Footer/slider-1.jpg" alt="" className="hide-on-desktop" />>
            <div className="hide-on-mobile">
              <video
                title="0"
                id="bgvid0"
                autoPlay
                loop
                muted
                poster="http://content.kyazoonga.com/Footer/slider-1.jpg"
              >
                <source src="http://content.kyazoonga.com/videos/video9.mp4" type="video/mp4" />
                >Your browser does not support the video tag.
              </video>
            </div>
          </div>
          <div data-slide="1" className="item">
            <img src="http://content.kyazoonga.com/Footer/slider-1.jpg" alt="" className="hide-on-desktop" />>
            <div className="hide-on-mobile">
              <video
                title="1"
                id="bgvid1"
                autoPlay
                loop
                muted
                poster="http://content.kyazoonga.com/Footer/slider-1.jpg"
              >
                <source src="http://content.kyazoonga.com/videos/video1.mp4" type="video/mp4" />
                >Your browser does not support the video tag.
              </video>
            </div>
          </div>
          <div data-slide="2" className="item">
            <img src="http://content.kyazoonga.com/Footer/slider-1.jpg" alt="" className="hide-on-desktop" />>
            <div className="hide-on-mobile">
              <video
                title="2"
                id="bgvid2"
                autoPlay
                loop
                muted
                poster="http://content.kyazoonga.com/Footer/slider-1.jpg"
              >
                <source src="http://content.kyazoonga.com/videos/video2.mp4" type="video/mp4" />
                >Your browser does not support the video tag.
              </video>
            </div>
          </div>
          <div data-slide="3" className="item">
            <img src="http://content.kyazoonga.com/Footer/slider-1.jpg" alt="" className="hide-on-desktop" />>
            <div className="hide-on-mobile">
              <video
                title="3"
                id="bgvid3"
                autoPlay
                loop
                muted
                poster="http://content.kyazoonga.com/Footer/slider-1.jpg"
              >
                <source src="http://content.kyazoonga.com/videos/video3.mp4" type="video/mp4" />
                >Your browser does not support the video tag.
              </video>
            </div>
          </div>
          <div data-slide="4" className="item">
            <img src="http://content.kyazoonga.com/Footer/slider-1.jpg" alt="" className="hide-on-desktop" />>
            <div className="hide-on-mobile">
              <video
                title="4"
                id="bgvid4"
                autoPlay
                loop
                muted
                poster="http://content.kyazoonga.com/Footer/slider-1.jpg"
              >
                <source src="http://content.kyazoonga.com/videos/video8.mp4" type="video/mp4" />
                >Your browser does not support the video tag.
              </video>
            </div>
          </div>
        </div>
        {/* <!-- end of '.carousel-inner' --> */}
      </div>
    );
  }
}

export default Slider;
