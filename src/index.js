import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { Router, Route, Switch } from 'react-router-dom';

import store from './store';
import history from './utils/history';

import './assets/css/site.css';

import Dashboard from './containers/dashboard';

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Router history={history}>
        <Switch>
          <Route path="/" component={Dashboard} />
        </Switch>
      </Router>
    </div>
  </Provider>,
  document.querySelector('#app'),
);
